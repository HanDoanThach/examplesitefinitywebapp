﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Varsity.SitefinityWebApp.Mvc.Models
{
    public class SocialInformationViewModel
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Google { get; set; }
        public string Linkin { get; set; }
        public string Youtube { get; set; }
    }
}