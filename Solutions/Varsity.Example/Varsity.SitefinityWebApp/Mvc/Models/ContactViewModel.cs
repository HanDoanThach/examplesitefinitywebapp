﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Varsity.SitefinityWebApp.Mvc.Models
{
    public class ContactViewModel
    {
        public Guid ContactId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }

    public class ContactPageViewModel
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string IFrameMap { get; set; }
        public ContactViewModel ContactViewModel { get; set; }
    }
}