﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Localization;

namespace Varsity.SitefinityWebApp.Mvc.StringResources
{
    [ObjectInfo(typeof(ContactResources),
       ResourceClassId = "ContactResources",
       Title = "ContactResourcesTitle",
       Description = "ContactResourcesDescription")]
    public class ContactResources : Resource
    {
        [ResourceEntry("ContactResourcesTitle",
            Value = "Contact Resources labels",
            Description = "The title of this class",
            LastModified = "2019/11/04")]
        public string ContactResourcesTitle
        {
            get
            {
                return this["ContactResourcesTitle"];
            }
        }

        /// <summary>
        /// Full path to the current page
        /// </summary>
        [ResourceEntry("ContactResourcesDescription",
            Value = "Contains localizable resources",
            Description = "The description of this class",
            LastModified = "2019/11/04")]
        public string ContactResourcesDescription
        {
            get
            {
                return this["ContactResourcesDescription"];
            }
        }
    }
}