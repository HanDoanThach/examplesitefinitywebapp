﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Varsity.SitefinityWebApp.Mvc.Models;
using Varsity.SitefinityWebApp.Utility;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Versioning;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Locations.Configuration;
using Telerik.Sitefinity.GeoLocations.Model;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Locations;
using Telerik.Sitefinity.Mvc;
using Telerik.OpenAccess;

namespace Varsity.SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "ExampleModule_MVC", Title = "ExampleModule", SectionName = "CustomWidgets")]
    public class ExampleModuleController : Controller
    {
        public ActionResult Index()
        {
            var exampleModules = RetrieveCollectionOfExampleModules();
            if (exampleModules != null && exampleModules.Any())
            {
                var viewModel = new ExampleModulePageViewModel()
                {
                    ExampleModuleViewModels = new List<ExampleModuleViewModel>()
                };
                foreach (var dynamicContent in exampleModules)
                {
                    viewModel.ExampleModuleViewModels.Add(new ExampleModuleViewModel()
                    {
                        ExampleId = dynamicContent.Id,
                        FirstName = dynamicContent.GetValue("firstname").ToString(),
                        LastName = dynamicContent.GetValue("lastname").ToString(),
                        Email = dynamicContent.GetValue("email").ToString(),
                        Country = GetCountryById(dynamicContent.GetValue<TrackedList<Guid>>("countries")),
                    });
                }

                return View(viewModel);
            }
            return View();
        }


        public ActionResult Create()
        {
            var viewModel = new ExampleModuleViewModel()
            {
                Birthday = DateTime.Now
            };
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var country = taxonomyManager.GetTaxa<FlatTaxon>().Where(t => t.Taxonomy.Name == SystemConstant.TaxonomyCountry);
            if (country != null)
            {
                viewModel.Countries = country.Select(c => new CountryViewModel()
                {
                    CountryId = c.Id,
                    Title = c.Title
                }).ToList();
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ExampleModuleViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CreateExampleModule(viewModel);
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                return HttpNotFound();
            }
        }

        private string GetCountryById(TrackedList<Guid> countryIds)
        {
            var taxonomyId = countryIds.FirstOrDefault();
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var country = taxonomyManager.GetTaxa<FlatTaxon>().FirstOrDefault(ft => ft.Id == taxonomyId);
            if (country != null)
            {
                return country.Title;
            }

            return null;
        }

        public ActionResult Update(string exampleId)
        {
            if (Guid.TryParse(exampleId, out Guid exampleModuleId))
            {
                var viewModel = new ExampleModuleViewModel();
                TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
                var country = taxonomyManager.GetTaxa<FlatTaxon>().Where(t => t.Taxonomy.Name == SystemConstant.TaxonomyCountry);
                if (country != null)
                {
                    viewModel.Countries = country.Select(c => new CountryViewModel()
                    {
                        CountryId = c.Id,
                        Title = c.Title
                    }).ToList();
                }
                var exampleModule = GetExampleModuleById(exampleModuleId);
                if (exampleModule != null)
                {
                    viewModel.FirstName = exampleModule.GetValue("firstname").ToString();
                    viewModel.LastName = exampleModule.GetString("lastname").ToString();
                    viewModel.Email = exampleModule.GetValue("email").ToString();
                    viewModel.Address = exampleModule.GetValue("address").ToString();
                    viewModel.ExampleId = exampleModuleId;
                    viewModel.Birthday = exampleModule.GetValue<DateTime>("birthday");
                    viewModel.CountryId = exampleModule.GetValue<TrackedList<Guid>>("countries").FirstOrDefault();
                    return View(viewModel);
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ExampleModuleViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UpdateExampleModule(viewModel);
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                return HttpNotFound();
            }
        }


        public IQueryable<DynamicContent> RetrieveCollectionOfExampleModules()
        {
            var providerName = string.Empty;

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
            Type examplemoduleType = TypeResolutionService.ResolveType(SystemConstant.ExampleModuleType);

            var myCollection = dynamicModuleManager.GetDataItems(examplemoduleType);
            return myCollection;
        }


        public DynamicContent GetExampleModuleById(Guid exampleId)
        {
            var providerName = string.Empty;

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
            Type examplemoduleType = TypeResolutionService.ResolveType(SystemConstant.ExampleModuleType);

            return dynamicModuleManager.GetDataItems(examplemoduleType).FirstOrDefault(em => em.Id == exampleId);
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(string exampleId)
        {
            try
            {
                return Json(DeleteExampleModule(exampleId), JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false);
            }
        }

        private void CreateExampleModule(ExampleModuleViewModel viewModel)
        {
            var providerName = string.Empty;
            var cultureName = SystemConstant.CultureNameEN;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
            Type examplemoduleType = TypeResolutionService.ResolveType(SystemConstant.ExampleModuleType);
            DynamicContent examplemoduleItem = dynamicModuleManager.CreateDataItem(examplemoduleType);

            CreateExampleModuleItem(dynamicModuleManager, examplemoduleItem, cultureName, viewModel);
        }


        private void UpdateExampleModule(ExampleModuleViewModel viewModel)
        {
            var examplemodule = GetExampleModuleById(viewModel.ExampleId);
            if (examplemodule != null)
            {
                var providerName = string.Empty;
                var cultureName = SystemConstant.CultureNameEN;
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
                CreateExampleModuleItem(dynamicModuleManager, examplemodule, cultureName, viewModel);
            }
        }

        private bool DeleteExampleModule(string exampleId)
        {
            if (Guid.TryParse(exampleId, out Guid exampleModuleId))
            {
                var examplemodule = GetExampleModuleById(exampleModuleId);
                if (examplemodule != null)
                {
                    var providerName = string.Empty;
                    var cultureName = SystemConstant.CultureNameEN;
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

                    DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
                    dynamicModuleManager.DeleteDataItem(examplemodule);
                    using (new ElevatedModeRegion(dynamicModuleManager))
                    {
                        dynamicModuleManager.SaveChanges();
                    }
                    return true;
                }
            }
            return false;
        }


        private void CreateExampleModuleItem(DynamicModuleManager dynamicModuleManager, DynamicContent examplemoduleItem, string cultureName, ExampleModuleViewModel viewModel)
        {
            examplemoduleItem.SetString("firstname", viewModel.FirstName, cultureName);
            examplemoduleItem.SetString("lastname", viewModel.LastName, cultureName);
            examplemoduleItem.SetString("email", viewModel.Email, cultureName);
            examplemoduleItem.SetValue("birthday", viewModel.Birthday);
            examplemoduleItem.SetString("address", viewModel.Address);
            examplemoduleItem.Organizer.ClearAll();
            examplemoduleItem.Organizer.AddTaxa("countries", viewModel.CountryId);

            examplemoduleItem.SetString("UrlName", FriendlyUrlHelper.GetFriendlyTitle(string.Format("{0} {1}", viewModel.FirstName, viewModel.LastName), true), cultureName);
            examplemoduleItem.SetValue("PublicationDate", DateTime.UtcNow);


            examplemoduleItem.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, WorkFlowStatus.Draft, new CultureInfo(cultureName));

            var versionManager = VersionManager.GetManager();
            versionManager.CreateVersion(examplemoduleItem, false);
            using (new ElevatedModeRegion(dynamicModuleManager))
            {
                dynamicModuleManager.SaveChanges();
            }

            DynamicContent checkOutexamplemoduleItem = dynamicModuleManager.Lifecycle.CheckOut(examplemoduleItem) as DynamicContent;
            DynamicContent checkInexamplemoduleItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutexamplemoduleItem) as DynamicContent;
            versionManager.CreateVersion(checkInexamplemoduleItem, false);
            using (new ElevatedModeRegion(dynamicModuleManager))
            {
                dynamicModuleManager.SaveChanges();
            }
        }

        public int PageSize { get; set; }
    }
}
