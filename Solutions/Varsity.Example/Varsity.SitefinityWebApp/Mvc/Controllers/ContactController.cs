﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Versioning;
using Varsity.SitefinityWebApp.Mvc.Models;
using Varsity.SitefinityWebApp.Utility;

namespace Varsity.SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Contact_MVC", Title = "Contact", SectionName = "CustomWidgets")]
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            var viewModel = new ContactPageViewModel()
            {
                Title = this.Title,
                Summary = this.Summary,
                IFrameMap = this.IFrameMap
            };
            return View(viewModel);
        }


        // GET: Contact/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contact/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContactViewModel viewModel)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    CreateContact(viewModel);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ContactViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UpdateContact(viewModel);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(ContactViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DeleteContact(viewModel);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        private void CreateContact(ContactViewModel viewModel)
        {
            var providerName = String.Empty;

            var transactionName = "someTransactionName";

            var cultureName = "en";
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
            Type contactType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Contacts.Contact");
            DynamicContent contactItem = dynamicModuleManager.CreateDataItem(contactType);

            CreateContactItem(dynamicModuleManager, contactItem, transactionName, viewModel);
        }


        private void UpdateContact(ContactViewModel viewModel)
        {
            var contact = RetrieveContactByID(viewModel.ContactId);
            if(contact != null)
            {
                var providerName = String.Empty;
                var transactionName = "someTransactionName";
                var cultureName = "en";
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
                CreateContactItem(dynamicModuleManager, contact, transactionName, viewModel);
            }
        }

        private void DeleteContact(ContactViewModel viewModel)
        {
            var contact = RetrieveContactByID(viewModel.ContactId);
            if (contact != null)
            {
                var providerName = String.Empty;
                var transactionName = "someTransactionName";
                var cultureName = "en";
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
                dynamicModuleManager.DeleteDataItem(contact);
                TransactionManager.CommitTransaction(transactionName);
            }
        }

        public DynamicContent RetrieveContactByID(Guid contactID)
        {
            var providerName = String.Empty;
            var transactionName = "someTransactionName";

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName, transactionName);
            Type contactType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Contacts.Contact");
            DynamicContent contactItem = dynamicModuleManager.CreateDataItem(contactType, contactID, "/DynamicModule");

            return contactItem;
        }

        private void CreateContactItem(DynamicModuleManager dynamicModuleManager, DynamicContent contactItem, string transactionName, ContactViewModel viewModel)
        {
            var cultureName = "en";
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

            contactItem.SetString("name", viewModel.Name, cultureName);
            contactItem.SetString("message", viewModel.Message, cultureName);
            contactItem.SetString("email", viewModel.Email, cultureName);
            contactItem.SetString("subject", viewModel.Subject, cultureName);


            contactItem.SetString("UrlName", FriendlyUrlHelper.GetFriendlyTitle(string.Format("{0} {1}","contact",viewModel.Name), true), cultureName);
            contactItem.SetValue("Owner", SecurityManager.GetCurrentUserId());
            contactItem.SetValue("PublicationDate", DateTime.UtcNow);


            contactItem.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, "Draft", new CultureInfo(cultureName));

            var versionManager = VersionManager.GetManager(null, transactionName);
            versionManager.CreateVersion(contactItem, false);
            TransactionManager.CommitTransaction(transactionName);

            DynamicContent checkOutContactItem = dynamicModuleManager.Lifecycle.CheckOut(contactItem) as DynamicContent;
            DynamicContent checkInContactItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutContactItem) as DynamicContent;
            versionManager.CreateVersion(checkInContactItem, false);
            TransactionManager.CommitTransaction(transactionName);
        }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string IFrameMap { get; set; }
    }
}
