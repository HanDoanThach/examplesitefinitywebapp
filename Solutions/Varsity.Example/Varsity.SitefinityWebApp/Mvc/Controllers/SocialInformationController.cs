﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Varsity.SitefinityWebApp.Mvc.Models;

namespace Varsity.SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "SocialInformation_MVC", Title = "Social Information", SectionName = "CustomWidgets")]
    public class SocialInformationController : Controller
    {
        // GET: SocialInformation
        public ActionResult Index()
        {
            var viewModel = new SocialInformationViewModel()
            {
                PhoneNumber = this.PhoneNumber,
                Email = this.Email,
                Facebook = this.Facebook,
                Twitter = this.Twitter,
                Google = this.Google,
                Linkin = this.Linkin,
                Youtube = this.Youtube
            };
            return View("Index",viewModel);
        }

        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Google { get; set; }
        public string Linkin { get; set; }
        public string Youtube { get; set; }
    }
}